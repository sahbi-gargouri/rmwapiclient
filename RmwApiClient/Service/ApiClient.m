//
//  ApiRequests.m
//  RMWCaissePro
//
//  Created by GARGOURI Sahbi on 29/10/2015.
//  Copyright © 2015 BNP. All rights reserved.
//

#import "ApiClient.h"
#import "AFHTTPRequestOperation.h"
#import "SharedClientSingleton.h"

static NSString * const grant_type = @"password";

static NSString * const TOKEN_PATH   = @"sso/token";
static NSString * const OPEN_TICKET   = @"pay-mlv/2.1/proximity/ticket/open";
static NSString * const PERFORM_PAYMENT   = @"pay-tm/2.1/proximity/payment/perform";
static NSString * const CHECK_PAYMENT   = @"pay-tm/2.1/proximity/payment/check";
static NSString * const CANCEL_PAYMENT   = @"pay-tm/2.1/proximity/payment/cancel";
static NSString * const CLOSE_TICKET   = @"pay-tm/2.1/proximity/ticket/close";
static NSString * const CANCEL_TICKET   = @"pay-tm/2.1/proximity/ticket/cancel";
static NSString * const CANCEL_IDENTIFICATION   = @"pay-tm/2.1/proximity/identification/cancel";


@implementation ApiClient

SharedClientSingleton *sharedClient;

-(id) init {
    if (self = [super init]){
        sharedClient = [SharedClientSingleton sharedClient];
    }
    return self;
}

- (id)initApiClientWithURL:(NSString *) url merchantName:(NSString *)merchantName merchantPassword:(NSString *)merchantPassword merchantEntrepriseCode:(NSString *)merchantEntrepriseCode merchantConsumerKey:(NSString *)merchantConsumerKey merchantConsumerSecret:(NSString *)merchantConsumerSecret merchantTillID:(NSString *)merchantTillID merchantStoreID:(NSString *)merchantStoreID merchantMCC:(NSString *)merchantMCC merchantPSPID:(NSString *)merchantPSPID merchantPSPMerchantID:(NSString *)merchantPSPMerchantID merchantPosID:(NSString *)merchantPosID apiBaseUrl:(NSString *)apiBaseUrl {
    
    [[NSUserDefaults standardUserDefaults] setObject:url forKey:@"RMWserverURL"];
    [[SharedClientSingleton sharedClient] setMerchantValuesWithName:merchantName merchantPassword:merchantPassword merchantEntrepriseCode:merchantEntrepriseCode merchantConsumerKey:merchantConsumerKey merchantConsumerSecret:merchantConsumerSecret merchantTillID:merchantTillID merchantStoreID:merchantStoreID merchantMCC:merchantMCC merchantPSPID:merchantPSPID merchantPSPMerchantID:merchantPSPMerchantID merchantPosID:merchantPosID apiBaseUrl:apiBaseUrl];
    sharedClient = [SharedClientSingleton sharedClient];
    return self;
}

- (void)getToken:(id)delegate {
    self.delegate = delegate;
    
    NSDictionary *params = @ {@"grant_type" :grant_type, @"username" :sharedClient.merchantName, @"password" :sharedClient.merchantPassword};

    sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
    sharedClient.requestSerializer = [AFHTTPRequestSerializer serializer];
    
    // Create NSData object
    NSData *nsdata = [[NSString stringWithFormat:@"%@:%@", sharedClient.merchantConsumerKey, sharedClient.merchantConsumerSecret] dataUsingEncoding:NSUTF8StringEncoding];
    
    // Get NSString from NSData object in Base64
    NSString *apiKey = [nsdata base64EncodedStringWithOptions:0];
    [sharedClient.requestSerializer setValue:[NSString stringWithFormat:@"Basic %@", apiKey] forHTTPHeaderField:@"Authorization"];
    [sharedClient.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type" ];
    
    [sharedClient POST:TOKEN_PATH parameters:params
       success:^(NSURLSessionDataTask *operation, id responseObject) {
           NSLog(@"JSON: %@ \n access_token :%@ \n token_type :%@ \n expires_in :%@", responseObject, responseObject[@"access_token"], responseObject[@"token_type"], responseObject[@"expires_in"]);
           [sharedClient setAccessTokenWithValue:responseObject[@"access_token"] expires_in:responseObject[@"expires_in"] token_type:responseObject[@"token_type"] timeTokenGet:[[NSDate date] timeIntervalSince1970]];
           if ([self.delegate respondsToSelector:@selector(apiClient:didSucceedWithResponse:)]) {
               [self.delegate apiClient:self didSucceedWithResponse:responseObject];
           }
       }
       failure: ^(NSURLSessionDataTask *operation, NSError *error) {
           NSLog(@"Error: %@", error);
           if ([self.delegate respondsToSelector:@selector(apiClient:didFailWithError:)]) {
               [self.delegate apiClient:self didFailWithError:error];
           }
       }];
}


- (BOOL)isTokenExpired {
    double currentTime = [[NSDate date] timeIntervalSince1970];
    if (currentTime - sharedClient.timeTokenGet > [sharedClient.expires_in doubleValue]) {
        return YES;
    }
    return NO;
}

- (void)callPOST:(NSString *)path withParams:(NSDictionary *)params {
    sharedClient.responseSerializer = [AFJSONResponseSerializer serializer];
    sharedClient.requestSerializer = [AFJSONRequestSerializer serializer];
    [sharedClient.requestSerializer setValue:[NSString stringWithFormat:@"%@ %@", @"Bearer", sharedClient.access_token] forHTTPHeaderField:@"Authorization"];
    [sharedClient POST:path parameters:params
          success:^(NSURLSessionDataTask *operation, id responseObject) {
              NSLog(@"success :%@", responseObject);
              if ([self.delegate respondsToSelector:@selector(apiClient:didSucceedWithResponse:)]) {
                  [self.delegate apiClient:self didSucceedWithResponse:responseObject];
              }
          }
          failure: ^(NSURLSessionDataTask *operation, NSError *error) {
              NSLog(@"Error: %@", error);
              if ([self.delegate respondsToSelector:@selector(apiClient:didFailWithError:)]) {
                  [self.delegate apiClient:self didFailWithError:error];
              }
          }
     ];
}


- (void)openTicket:(id)delegate withId:(NSString *)ticketId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"12345678910" forKey:@"invocationId"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:@ {@"ticketId" :ticketId, @"orderId" :@"05"} forKey:@"ticket"];
    [parameters setValue:@ {@"subscriberCode" :rmwId, @"identificationType" :@"01"} forKey:@"customer"];
    [parameters setValue:@ {@"posId" :sharedClient.merchantPosID, @"enterpriseCode" :sharedClient.merchantEntrepriseCode, @"tillId" :sharedClient.merchantTillID, @"storeId" :sharedClient.merchantStoreID} forKey:@"merchant"];
    [parameters setValue:@[] forKey:@"categoryList"];
    NSLog(@"%@", parameters);
    [self callPOST:OPEN_TICKET withParams:parameters];
}

- (void)performPayment:(id)delegate withTotalAmountBeforeDiscount:(float)totalAmount loyaltyAmount:(float)loyaltyAmount couponsAmount:(float)couponsAmount ticketId:(NSString *)ticketId rmwId:(NSString *)rmwId merchantPaymentId:(NSString *)merchantPaymentId specificPaymentList:(NSArray *) specificPaymentList{
    self.delegate = delegate;
    
    float netAmount = totalAmount - loyaltyAmount - couponsAmount;
    NSString *netAmountNormalized = [[NSString stringWithFormat:@"%.2f", netAmount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *totalAmountNormalized = [[NSString stringWithFormat:@"%.2f", totalAmount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *loyaltyAmountNormalized = [[NSString stringWithFormat:@"%.2f", loyaltyAmount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSString *couponsAmountNormalized = [[NSString stringWithFormat:@"%.2f", couponsAmount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [parameters setValue:@ {@"enterpriseCode" :sharedClient.merchantEntrepriseCode, @"mcc": sharedClient.merchantMCC, @"pspId": sharedClient.merchantPSPID, @"pspMerchantId": sharedClient.merchantPSPMerchantID} forKey:@"merchant"];
    [parameters setValue:@ {@"captureDate" :stringDate, @"operationType" :@"CAP"} forKey:@"pspOptions"];
    [parameters setValue:@ {@"currency" :@"EUR", @"exponent" :@"2", @"netAmount" :netAmountNormalized, @"orderId" :@"05", @"remainingAmount" :netAmountNormalized, @"rmwCouponAmount" :couponsAmountNormalized, @"rmwLoyaltyAmount" :loyaltyAmountNormalized, @"ticketId" :ticketId, @"totalAmount" :totalAmountNormalized} forKey:@"ticket"];

    if(specificPaymentList.count > 0){
        NSMutableArray *paymentParametersArray = [[NSMutableArray alloc] init];
        for (SpecificPaymentType * specificPaymentType in specificPaymentList) {
            NSMutableDictionary *paymentParameters = [[NSMutableDictionary alloc] init];
            [paymentParameters setValue:specificPaymentType.paymentType forKey:@"spPaymentType"];
            NSString *maxAmountNormalized = [[NSString stringWithFormat:@"%.2f", [specificPaymentType.maxAmount floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@""];
            [paymentParameters setValue:maxAmountNormalized forKey:@"maxAmount"];
            [paymentParameters setValue:@"1" forKey:@"priority"];
            [paymentParameters setValue:@"EUR" forKey:@"currency"];
            NSDictionary *attributes = [[NSDictionary alloc] initWithObjectsAndKeys:@"123", @"attributeKey", specificPaymentType.attributeValue, @"attributeValue", nil];
            NSArray *attributesArray = [[NSArray alloc] initWithObjects:attributes, nil];
            [paymentParameters setValue:attributesArray  forKey:@"attributesList"];
            [paymentParametersArray addObject:paymentParameters];
        }
        
        [parameters setValue:@ {@"amountToPay" :netAmountNormalized, @"specificPaymentList" :paymentParametersArray, @"merchantPaymentId" :merchantPaymentId, @"purchaseDate" :stringDate} forKey:@"payment"];
    } else {
        [parameters setValue:@ {@"amountToPay" :netAmountNormalized, @"merchantPaymentId" :merchantPaymentId, @"purchaseDate" :stringDate} forKey:@"payment"];
    }
    
    [self callPOST:PERFORM_PAYMENT withParams:parameters];
}

- (void)checkPayment:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPaymentId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"ticketId" :ticketId} forKey:@"ticket"];
    [parameters setValue:@ {@"enterpriseCode" :sharedClient.merchantEntrepriseCode} forKey:@"merchant"];
    [parameters setValue:@ {@"merchantPaymentId" :merchantPaymentId} forKey:@"payment"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [self callPOST:CHECK_PAYMENT withParams:parameters];
}


- (void)cancelPayment:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPaymentId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"12345678910" forKey:@"invocationId"];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"merchantPaymentId" :merchantPaymentId} forKey:@"payment"];
    [parameters setValue:@ {@"ticketId" :ticketId} forKey:@"ticket"];
    [parameters setValue:@ {@"enterpriseCode" :sharedClient.merchantEntrepriseCode} forKey:@"merchant"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [self callPOST:CANCEL_PAYMENT withParams:parameters];
}

- (void)closeTicket:(id)delegate withTicketId:(NSString *)ticketId transactionDate:(NSDate *)transactionDate merchantPaymentId:(NSString *)merchantPaymentId amount:(float)amount rmwId:(NSString *)rmwId advantageListUsed:(NSArray *)advantageListUsed transactionList:(NSArray *)transactionList {
    
    NSString *amountNormalized = [[NSString stringWithFormat:@"%.2f", amount] stringByReplacingOccurrencesOfString:@"." withString:@""];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    NSString *stringTransactionDate = [df stringFromDate:transactionDate];
    
    self.delegate = delegate;
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    
    NSMutableArray *transactions = [[NSMutableArray alloc] init];
    for (RMWTransaction *transaction in transactionList) {
        NSMutableDictionary *transactionParameters = [[NSMutableDictionary alloc] init];
        [transactionParameters setValue:@"2" forKey:@"exponent"];
        [transactionParameters setValue:@"EUR" forKey:@"currency"];
        [transactionParameters setValue:@0 forKey:@"tagNonRMWPayment"];
        [transactionParameters setValue:merchantPaymentId forKey:@"merchantPaymentId"];
        [transactionParameters setValue:stringTransactionDate forKey:@"transactionDate"];
        [transactionParameters setValue:transaction.amountPaid forKey:@"amountPaid"];
        [transactionParameters setValue:transaction.transactionType forKey:@"transactionType"];
        [transactionParameters setValue:transaction.transactionId forKey:@"transactionId"];
        [transactions addObject:transactionParameters];
    }
    
    [parameters setValue:transactions forKey:@"transactionList"];
    [parameters setValue:@"OK" forKey:@"merchantFeedback"];
    [parameters setValue:stringTransactionDate forKey:@"merchantActionDate"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [parameters setValue:@ {@"enterpriseCode" :sharedClient.merchantEntrepriseCode} forKey:@"merchant"];
    [parameters setValue:@ {@"ticketId" :ticketId, @"orderId" :@"05", @"netAmount" :amountNormalized, @"currency" :@"EUR", @"exponent" :@"2"} forKey:@"ticket"];
    [parameters setValue:@1 forKey:@"degratedMode"];
    [parameters setValue:advantageListUsed forKey:@"advantageListUsed"];
    [self callPOST:CLOSE_TICKET withParams:parameters];
}

- (void)cancelTicket:(id)delegate withAmount:(float)amount ticketId:(NSString *)ticketId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:stringDate forKey:@"merchantActionDate"];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:@1 forKey:@"degratedMode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"ticketId" :@"NAME_12_9876", @"orderId" :@"05"} forKey:@"ticket"];
    [parameters setValue:@ {@"subscriberCode" :@"3614569818746123456"} forKey:@"customer"];
    [parameters setValue:@ {@"enterpriseCode" :sharedClient.merchantEntrepriseCode, @"tillId" :sharedClient.merchantTillID, @"storeId" :sharedClient.merchantStoreID} forKey:@"merchant"];
    [self callPOST:CANCEL_TICKET withParams:parameters];
}

- (void)cancelIdentification:(id)delegate withTicketId:(NSString *)ticketId rmwId:(NSString *)rmwId {
    self.delegate = delegate;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZ"];
    NSString *stringDate = [df stringFromDate:[NSDate date]];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:stringDate forKey:@"merchantActionDate"];
    [parameters setValue:@"RMWSP" forKey:@"serviceProviderCode"];
    [parameters setValue:@1 forKey:@"degratedMode"];
    [parameters setValue:stringDate forKey:@"timestamp"];
    [parameters setValue:@ {@"ticketId" :ticketId, @"orderId" :@"05"} forKey:@"ticket"];
    [parameters setValue:@ {@"subscriberCode" :rmwId} forKey:@"customer"];
    [parameters setValue:@ {@"posId" :sharedClient.merchantPosID, @"enterpriseCode" :sharedClient.merchantEntrepriseCode, @"tillId" :sharedClient.merchantTillID, @"storeId" :sharedClient.merchantStoreID} forKey:@"merchant"];
    [self callPOST:CANCEL_IDENTIFICATION withParams:parameters];
}

@end
