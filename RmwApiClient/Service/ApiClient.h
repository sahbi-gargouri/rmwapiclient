//
//  ApiRequests.h
//  RMWCaissePro
//
//  Created by GARGOURI Sahbi on 29/10/2015.
//  Copyright © 2015 BNP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RMWTransaction.h"
#import "SpecificPaymentType.h"

@protocol ApiClientDelegate;

@interface ApiClient : NSObject

@property (nonatomic, weak) id<ApiClientDelegate>delegate;

// Initialisation Methods
- (id)initApiClientWithURL:(NSString *) url merchantName:(NSString *)merchantName merchantPassword:(NSString *)merchantPassword merchantEntrepriseCode:(NSString *)merchantEntrepriseCode merchantConsumerKey:(NSString *)merchantConsumerKey merchantConsumerSecret:(NSString *)merchantConsumerSecret merchantTillID:(NSString *)merchantTillID merchantStoreID:(NSString *)merchantStoreID merchantMCC:(NSString *)merchantMCC merchantPSPID:(NSString *)merchantPSPID merchantPSPMerchantID:(NSString *)merchantPSPMerchantID merchantPosID:(NSString *)merchantPosID apiBaseUrl:(NSString *)apiBaseUrl;

- (void)callPOST:(NSString *)path withParams:(NSDictionary *)params;
- (void)getToken:(id)delegate;
- (void)openTicket:(id)delegate withId:(NSString *)ticketId rmwId:(NSString *)rmwId;
- (void)performPayment:(id)delegate withTotalAmountBeforeDiscount:(float)totalAmount loyaltyAmount:(float)loyaltyAmount couponsAmount:(float)couponsAmount ticketId:(NSString *)ticketId rmwId:(NSString *)rmwId merchantPaymentId:(NSString *)merchantPaymentId specificPaymentList:(NSArray *) specificPaymentList;
- (void)cancelIdentification:(id)delegate withTicketId:(NSString *)ticketId rmwId:(NSString *)rmwId;
- (void)checkPayment:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPaymentId rmwId:(NSString *)rmwId;
- (void)closeTicket:(id)delegate withTicketId:(NSString *)ticketId transactionDate:(NSDate *)transactionDate merchantPaymentId:(NSString *)merchantPaymentId amount:(float)amount rmwId:(NSString *)rmwId advantageListUsed:(NSArray *)advantageListUsed transactionList:(NSArray *)transactionList;
- (void)cancelPayment:(id)delegate withTicketId:(NSString *)ticketId merchantPaymentId:(NSString *)merchantPaymentId rmwId:(NSString *)rmwId;

@end

#pragma mark - Protocol Methods

@protocol ApiClientDelegate <NSObject>

@optional

// RETDocumentsTableViewController
- (void)apiClient:(ApiClient *)sharedClient didSucceedWithResponse:(id)responseObject;
- (void)apiClient:(ApiClient *)sharedClient didSucceedWithOperations:(NSArray *)operations;
- (void)apiClient:(ApiClient *)sharedClient didFailWithError:(NSError *)error;

@end