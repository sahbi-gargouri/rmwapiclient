//
//  Transaction.h
//  RmwApiClient
//
//  Created by Steeve Guillaume on 26/04/2016.
//  Copyright © 2016 BNP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RMWTransaction : NSObject

@property (nonatomic, retain) NSString *transactionId;
@property (nonatomic, retain) NSString *transactionType;
@property (nonatomic, retain) NSString *amountPaid;
@property (nonatomic, retain) NSString *transactionNumber;
@property (nonatomic, retain) NSString *transactionStatus;

@end
