//
//  Transaction.m
//  RmwApiClient
//
//  Created by Steeve Guillaume on 26/04/2016.
//  Copyright © 2016 BNP. All rights reserved.
//

#import "RMWTransaction.h"

@implementation RMWTransaction

- (id) initWithTransactionId:(NSString *)transactionId transactionType:(NSString *)transactionType amountPaid:(NSString *)amountPaid transactionNumber:(NSString *)transactionNumber transactionStatus:(NSString *)transactionStatus{
   
   self.transactionId = transactionId;
   self.transactionType = transactionType;
   self.amountPaid = amountPaid;
   self.transactionNumber = transactionNumber;
   self.transactionStatus = transactionStatus;
   
    
    return self;
}

@end
