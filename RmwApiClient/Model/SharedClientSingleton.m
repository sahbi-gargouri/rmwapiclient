//
//  SharedClientSingleton.m
//  RmwApiClient
//
//  Created by Steeve Guillaume on 27/04/2016.
//  Copyright © 2016 BNP. All rights reserved.
//

#import "SharedClientSingleton.h"

@implementation SharedClientSingleton

static SharedClientSingleton *_sharedClient = nil;

+ (SharedClientSingleton *)sharedClient
{
    static SharedClientSingleton *_sharedHTTPClient = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] objectForKey:@"RMWserverURL"]]];
    });
    
    return _sharedHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        //self.requestSerializer = [AFJSONRequestSerializer serializer];
    }
    
    return self;
}

- (void)setMerchantValuesWithName:(NSString *)merchantName merchantPassword:(NSString *)merchantPassword merchantEntrepriseCode:(NSString *)merchantEntrepriseCode merchantConsumerKey:(NSString *)merchantConsumerKey merchantConsumerSecret:(NSString *)merchantConsumerSecret merchantTillID:(NSString *)merchantTillID merchantStoreID:(NSString *)merchantStoreID merchantMCC:(NSString *)merchantMCC merchantPSPID:(NSString *)merchantPSPID merchantPSPMerchantID:(NSString *)merchantPSPMerchantID merchantPosID:(NSString *)merchantPosID apiBaseUrl:(NSString *)apiBaseUrl {
    self.merchantName = merchantName;
    self.merchantPassword = merchantPassword;
    self.merchantEntrepriseCode = merchantEntrepriseCode;
    self.merchantConsumerKey = merchantConsumerKey;
    self.merchantConsumerSecret = merchantConsumerSecret;
    self.merchantMCC = merchantMCC;
    self.merchantTillID = merchantTillID;
    self.merchantStoreID = merchantStoreID;
    self.merchantPSPID = merchantPSPID;
    self.merchantPSPMerchantID = merchantPSPMerchantID;
    self.merchantPosID = merchantPosID;
    self.apiBaseUrl = apiBaseUrl;
}

- (void)setAccessTokenWithValue:(NSString *)access_token expires_in:(NSString *)expires_in token_type:(NSString *)token_type timeTokenGet:(double)timeTokenGet {
    self.access_token = access_token;
    self.expires_in = expires_in;
    self.token_type = token_type;
    self.timeTokenGet = timeTokenGet;
}

@end
