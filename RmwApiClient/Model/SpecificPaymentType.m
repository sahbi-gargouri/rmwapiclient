//
//  SpecificPaymentType.m
//  RmwApiClient
//
//  Created by Steeve Guillaume on 26/04/2016.
//  Copyright © 2016 BNP. All rights reserved.
//

#import "SpecificPaymentType.h"

@implementation SpecificPaymentType

- (id) initWithPaymentType:(NSString *)paymentType maxAmount:(NSString *)maxAmount attributeValue:(NSString *)attributeValue{
    self.paymentType = paymentType;
    self.maxAmount = maxAmount;
    self.attributeValue = attributeValue;
    return self;
}

@end
