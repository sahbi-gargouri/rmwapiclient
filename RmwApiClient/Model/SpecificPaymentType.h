//
//  SpecificPaymentType.h
//  RmwApiClient
//
//  Created by Steeve Guillaume on 26/04/2016.
//  Copyright © 2016 BNP. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SpecificPaymentType : NSObject

@property (nonatomic, retain) NSString * paymentType;
@property (nonatomic, retain) NSString * maxAmount;
@property (nonatomic, retain) NSString * attributeValue;

- (id) initWithPaymentType:(NSString *)paymentType maxAmount:(NSString *)maxAmount attributeValue:(NSString *)attributeValue;

@end
