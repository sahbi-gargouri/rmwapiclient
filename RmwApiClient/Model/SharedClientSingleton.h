//
//  SharedClientSingleton.h
//  RmwApiClient
//
//  Created by Steeve Guillaume on 27/04/2016.
//  Copyright © 2016 BNP. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface SharedClientSingleton : AFHTTPSessionManager

@property (nonatomic) NSString *access_token;
@property (nonatomic) NSString *expires_in;
@property (nonatomic) NSString *token_type;
@property (nonatomic) double timeTokenGet;

@property (nonatomic) NSString *apiBaseUrl;
@property (nonatomic) NSString *merchantName;
@property (nonatomic) NSString *merchantPassword;
@property (nonatomic) NSString *merchantEntrepriseCode;
@property (nonatomic) NSString *merchantConsumerKey;
@property (nonatomic) NSString *merchantConsumerSecret;
@property (nonatomic) NSString *merchantTillID;
@property (nonatomic) NSString *merchantStoreID;
@property (nonatomic) NSString *merchantMCC;
@property (nonatomic) NSString *merchantPSPID;
@property (nonatomic) NSString *merchantPSPMerchantID;
@property (nonatomic) NSString *merchantPosID;

+ (SharedClientSingleton *)sharedClient;
- (instancetype)initWithBaseURL:(NSURL *)url;
- (void)setMerchantValuesWithName:(NSString *)merchantName merchantPassword:(NSString *)merchantPassword merchantEntrepriseCode:(NSString *)merchantEntrepriseCode merchantConsumerKey:(NSString *)merchantConsumerKey merchantConsumerSecret:(NSString *)merchantConsumerSecret merchantTillID:(NSString *)merchantTillID merchantStoreID:(NSString *)merchantStoreID merchantMCC:(NSString *)merchantMCC merchantPSPID:(NSString *)merchantPSPID merchantPSPMerchantID:(NSString *)merchantPSPMerchantID merchantPosID:(NSString *)merchantPosID apiBaseUrl:(NSString *)apiBaseUrl;
- (void)setAccessTokenWithValue:(NSString *)access_token expires_in:(NSString *)expires_in token_type:(NSString *)token_type timeTokenGet:(double)timeTokenGet;

@end
